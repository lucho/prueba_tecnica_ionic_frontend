import { Component, OnInit, ViewChild } from '@angular/core';
import { IonInfiniteScroll } from '@ionic/angular';
import { StorageService } from "../../services/storage/storage.service";
import { debounceTime } from "rxjs/operators";
import {
  NavController,
} from "@ionic/angular";

@Component({
  selector: "app-home",
  templateUrl: "./home.page.html",
  styleUrls: ["./home.page.scss"],
})
export class HomePage implements OnInit {
  @ViewChild( IonInfiniteScroll ) inifiteScroll: IonInfiniteScroll;
  public featuredProducts = [];
  public bestSellProducts = [];

  /**
   *
   */
  public dependences: any;
  public cartWithProduct: boolean = false;
  public user: any = null;
  public term: any = null;
  public page: number = 1;
  public data:any = [];

  constructor(
    private _storageServices: StorageService,
    private _navController: NavController,
    private _sotarageService: StorageService
  ) {}

  async ngOnInit() {
    this.user = await this._sotarageService.getUser();
    this._storageServices
      .watch()
      .pipe(debounceTime(500))
      .subscribe((data: any) => {
        if (data) {
          this.cartWithProduct = true;
        } else {
          this.cartWithProduct = false;
        }
      });
    this._storageServices
      .watchUser()
      .pipe(debounceTime(500))
      .subscribe((data: any) => {
        if (data) {
          this.user = data;
        }
      });
  }

  closeSession() {
    this._sotarageService.removeUser();
    this._sotarageService.removeToken();
    this._navController.navigateRoot(`welcome`, {
      animated: true,
      animationDirection: "forward",
    });
  }

  /**
   * Consulta del bucandor
   * @param term
   */

 async  onChange(event) {
   
  }


  async loadData( event ) {
   
    this.inifiteScroll.complete();
  }
}
