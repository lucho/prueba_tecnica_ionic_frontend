import { Component, OnInit } from "@angular/core";
import {FormGroup, Validators,FormBuilder } from "@angular/forms";
import { AuthenticationService } from '../../services/security/authentication.service';
import { User } from "src/app/interfaces/user";
import { UiServiceService } from '../../services/ui/ui-service.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: "app-signup",
  templateUrl: "./signup.page.html",
  styleUrls: ["./signup.page.scss"],
})
export class SignupPage implements OnInit {
 
  public userRegisterForm: FormGroup;

  constructor(private authenticationService : AuthenticationService, private uiService: UiServiceService,private navCtrl: NavController, private formBuilder: FormBuilder ) {}

  ngOnInit() {
    this.userRegisterForm = this.buildUserRegisterForm();
  }

  /**
   * @description Tiene como objetivo construir el formulario reactivo que va a tener esta pagina
   * @author Luis Garizabalo <luisf7ll28@gmail.com>
   * @date 2021-10-23
   * @param void
   * @returns FormGroup
   */
   private buildUserRegisterForm(): FormGroup {
    return this.formBuilder.group({
      'email': ['', Validators.required],
      'password': ['', Validators.required],
      'name': ['', Validators.required]

    });
  }

  /**
   * @description Tiene como objetivo obtener los datos que se enviararn al endPoint que registra al usaurio en Firebase
   * @author Luis Garizabalo <luisf7ll28@gmail.com>
   * @date 2021-10-23
   * @param void
   * @returns FormGroup
   */
  async register() {
    const user:User =  this.userRegisterForm.value;
    const valido = await this.authenticationService.registerUser(user);
    if( valido ){
      this.uiService.alertaInformativa(`Usuario registrado correctamente`,`Sesión`,`Iniciar Sesión`);
      this.navCtrl.navigateRoot(`home`,{ animated: true,animationDirection: 'forward'});
    }else{
      this.uiService.alertaInformativa(`No pudimos registrar tus datos`,`Error`,``);
    }
  }

}
