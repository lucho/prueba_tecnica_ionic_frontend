import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { AuthenticationService } from '../../services/security/authentication.service';
import { UiServiceService } from '../../services/ui/ui-service.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { User } from 'src/app/interfaces/user';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  public reactiveForm: FormGroup;

  constructor(private navCtrl: NavController,  private authenticationService : AuthenticationService, private uiService: UiServiceService, private formBuilder: FormBuilder ) { }

  ngOnInit() {
    this.reactiveForm = this.buildReactiveForm();
  }

  /**
   * @description Tiene como objetivo construir el formulario reactivo que va a tener esta pagina
   * @author Luis Garizabalo <luisf7ll28@gmail.com>
   * @date 2021-10-23
   * @param void
   * @returns FormGroup
   */
   private buildReactiveForm(): FormGroup {
    return this.formBuilder.group({
      'email': ['', Validators.required],
      'password': ['', Validators.required]
    });
  }


  /**
   * @description Tiene como objetivo obtener los datos que se enviararn al endPoint que valida el logueo del  usaurio en Firebase
   * @author Luis Garizabalo <luisf7ll28@gmail.com>
   * @date 2021-10-23
   * @returns void
   */
  async login() {
    const user: User  = this.reactiveForm.value;
    const valido = await  this.authenticationService.login(user);
    if( valido ){
      this.navCtrl.navigateRoot(`home`,{ animated: true,animationDirection: 'forward'});
    }else{
      this.uiService.alertaInformativa(`Usuario y contraseña incorrectos`,`Sesión`,`inválida`);
    }
  }

}
