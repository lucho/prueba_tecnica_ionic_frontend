import { Injectable } from '@angular/core';
import { AlertController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class UiServiceService {

  constructor( private alertController: AlertController) { }


  /**
   * @description Tiene como objetivo mostrar el mensaje informativo al cliente
   * @author Luis Garizabalo <luisf7ll28@gmail.com>
   * @date 2021-10-23
   * @param message
   * @param header
   * @param subHeader
   * @returns AlertController
   */
  async alertaInformativa( message: string,header = '', subHeader= '') {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: header,
      subHeader: subHeader,
      message: message,
      buttons: ['OK']
    });
    await alert.present();
  }

}
